import React from 'react';
import ReactDom from 'react-dom';
import {Switch, Router, Route, Redirect} from 'react-router-dom';
import {createBrowserHistory} from 'history';
import LoginComponent from './components/login/LoginComponent';
import './index.less';

const history = createBrowserHistory();

ReactDom.render(
  <div>
    <Router history={history}>
      <Switch>
        <Route path='/login' name='login' component={LoginComponent}/>
        <Route path='/app' name='app' component={LoginComponent}/>
        <Redirect to='/login'/>
      </Switch>
    </Router>
  </div>
  ,
  document.getElementById('root')
);
